{{- define "base-KubeadmConfigTemplateSpec" }}
{{- $envAll := index . 0 -}}
{{- $machine_kubelet_extra_args := index . 1 -}}
{{- $machine_additional_files := index . 2 -}}
{{- $machine_infra_provider := index . 3 -}}

joinConfiguration:
  nodeRegistration:
    kubeletExtraArgs: {{ mergeOverwrite (deepCopy $envAll.Values.kubelet_extra_args) $machine_kubelet_extra_args | include "quote-dict-values" | nindent 6 }}
ntp: {{ $envAll.Values.ntp | toYaml | nindent 2 }}
preKubeadmCommands:
  {{- if (include "k8s-version-match" (tuple ">=1.29.0" $envAll.Values.k8s_version)) }}
  - ln -s /etc/kubernetes/admin.conf /etc/kubernetes/kube-vip.admin.conf
  {{- end }}
  - | {{- include "kubeadm-alias-commands" (tuple "md") | nindent 2 }}
  - echo "Preparing Kubeadm bootstrap" > /var/log/my-custom-file.log
  - | {{ include "kernel-inotify-limits" $envAll | nindent 4 }}
  - | {{ include "containerd-config.toml-registry-config" $envAll | nindent 4 }}
files:
{{- $kubeadmctfiles := list -}}
{{- if ($envAll.Values.registry_mirrors | dig "hosts_config" "") -}}
    {{-  $kubeadmctfiles = include "registry_mirrors" $envAll | append $kubeadmctfiles -}}
{{- end }}
{{- if $envAll.Values.proxies.http_proxy }}
    {{-  $kubeadmctfiles = include "containerd_proxy_conf" $envAll | append $kubeadmctfiles -}}
{{- end }}
{{- $additional_files := mergeOverwrite (deepCopy $envAll.Values.additional_files) $machine_additional_files }}
{{- if $additional_files }}
    {{- $kubeadmctfiles = tuple $envAll $additional_files | include "additional_files" | append $kubeadmctfiles -}}
{{- end }}
{{- if $kubeadmctfiles }}
    {{- range $kubeadmctfiles -}}
        {{ . | indent 2 }}
    {{- end }}
{{- else }}
        []
{{- end }}
postKubeadmCommands:
  {{- if $envAll.Values.enable_longhorn }}
  - | {{ tuple $machine_infra_provider "cabpk" "md" | include "shell-longhorn-node-metadata" | nindent 4 }}
  {{- end }}
{{- end }}
