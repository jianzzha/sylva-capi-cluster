{{- define "KubeadmConfigTemplateSpec" -}}
  {{- $envAll := index . 0 -}}
  {{- $machine_deployment_def := index . 1 -}}

  {{- $machine_kubelet_extra_args := $machine_deployment_def.kubelet_extra_args -}}
  {{- $machine_kubeadm := $machine_deployment_def.kubeadm -}}
  {{- $machine_additional_commands := $machine_deployment_def.additional_commands -}}
  {{- $machine_additional_files := $machine_deployment_def.additional_files -}}
  {{- $machine_infra_provider := $machine_deployment_def.infra_provider -}}

  {{/*********** Initialize the components of the KubeadmConfigTemplate.spec.template.spec fields */}}
  {{- $base := tuple $envAll $machine_kubelet_extra_args $machine_additional_files $machine_infra_provider | include "base-KubeadmConfigTemplateSpec" | fromYaml }}
  {{- $infra := include (printf "%s-KubeadmConfigTemplateSpec" $machine_infra_provider) $envAll | fromYaml }}

joinConfiguration: {{ mergeOverwrite (deepCopy $base.joinConfiguration) $infra.joinConfiguration | toYaml | nindent 2 }}
{{- if $envAll.Values.ntp }}
ntp: {{ mergeOverwrite (deepCopy $base.ntp) $infra.ntp | toYaml | nindent 2 }}
{{- end }}
{{- $md_additional_commands := deepCopy ($envAll.Values.additional_commands | default dict) -}}
{{- if $machine_additional_commands }}
  {{- tuple $md_additional_commands $machine_additional_commands | include "merge-append" }}
{{- end }}
  {{- $base_merged_infra := dict }}
  {{- tuple $base_merged_infra $base $infra | include "merge-append" }}
preKubeadmCommands:
  - |
    cat <<'EOF' > /usr/local/bin/preKubeadmCommands.sh
    #!/bin/bash
    set -e
    set -o pipefail
    {{- range $base_merged_infra.preKubeadmCommands }}
      {{ . | nindent 4 }}
    {{- end }}
    {{- if $md_additional_commands.pre_bootstrap_commands }}
      {{- range $md_additional_commands.pre_bootstrap_commands }}
         {{ . | nindent 4 }}
       {{- end }}
    {{- end }}
    EOF
  - bash /usr/local/bin/preKubeadmCommands.sh
files: {{ concat $base.files $infra.files | default list | toYaml | nindent 2 }}
users: {{ mergeOverwrite (deepCopy ($envAll.Values.kubeadm | default dict)) $machine_kubeadm | dig "users" list | toYaml | nindent 2 }}
postKubeadmCommands:
  - |
    cat <<'EOF' > /usr/local/bin/postKubeadmCommands.sh
    #!/bin/bash
    set -e
    set -o pipefail
    {{- range $base_merged_infra.postKubeadmCommands }}
      {{ . | nindent 4 }}
    {{- end }}
    {{- if $md_additional_commands.post_bootstrap_commands }}
      {{- range $md_additional_commands.post_bootstrap_commands }}
    {{ . | nindent 4 }}
      {{- end }}
    {{- end }}
    EOF
  - bash /usr/local/bin/postKubeadmCommands.sh
{{- end }}
